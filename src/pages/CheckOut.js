import React from 'react'
import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import { Fragment, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CheckOut() {

	const { user } = useContext(UserContext);

	const [fullName, setFullName] = useState('');
	const [productName, setProductName] = useState('');
	const [quantity, setQuantity] = useState('');
	const navigate = useNavigate();
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (fullName !== '' && productName !== '' && quantity !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [fullName, productName, quantity])

	function addToCart(event) {
		event.preventDefault();

		fetch(`https://e-commerce-ma2l.onrender.com/order`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				fullName: fullName,
				productName: productName,
				quantity: quantity
			})
		})
			.then(result => result.json())
			.then(data => {
				// console.log(data)

				if (data) {
					Swal.fire({
						title: 'Order successfully created!',
						icon: 'success',
					})
					navigate("/products/active");
				} else {

					Swal.fire({
						title: 'Error occured! Please try again.',
						icon: 'error',
					})
				}
			})
			.catch(err => console.log(err))
	}
	return (
		<Container>
			<Row className="mt-5">
				<Col className='addP col-md-4 col-10 ms-auto p-3'>
					<Fragment>
						<h1 className='text-center'>Add to Cart</h1>
						<Form className='mt-4' onSubmit={event => addToCart(event)}>
							<Form.Group className="mb-3" controlId="formBasicFullName">
								<Form.Label>Full Name</Form.Label>
								<Form.Control
									type="fullName"
									placeholder="Enter your full name"
									value={fullName}
									onChange={event => setFullName(event.target.value)}
									required
								/>
							</Form.Group>
							<Form.Group className="mb-3" controlId="formBasicProductName">
								<Form.Label>Product Name</Form.Label>
								<Form.Control
									type="productName"
									placeholder="Enter product name"
									value={productName}
									onChange={event => setProductName(event.target.value)}
									required
								/>
							</Form.Group>
							<Form.Group className="mb-3" controlId="formBasicQuantity">
								<Form.Label>Quantity</Form.Label>
								<Form.Control
									type="quantity"
									placeholder="Enter quantity"
									value={quantity}
									onChange={event => setQuantity(event.target.value)}
									required
								/>
							</Form.Group>
							{
								user ?
									<Button variant="secondary" type="submit" className="mx-auto">
										Submit
									</Button>
									:
									<Button variant="dark" type="submit" disabled>
										Submit
									</Button>
							}
						</Form>
					</Fragment>
				</Col>
			</Row>
		</Container>
	);
}
