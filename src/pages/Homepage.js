import React from 'react'
import { Col, Container, Row, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function Homepage() {
  return (
    <Container >
      <Row>
        <Col className='col-md-5 col-10 ms-auto fluid p-5 text-center'>

          <h2 className='mt-5 text-light'>Welcome to JavaSweet!</h2>
          <p className='mt-3 text-light mb-2'>A Rare Experience in Taste</p>
  
        </Col>
      </Row>
    </Container>

  )
}
