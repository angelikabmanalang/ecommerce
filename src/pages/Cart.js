import OrderCard from '../components/OrderCard.js';
import { Fragment, useEffect, useState, useContext } from 'react';
import { Container, Row } from 'react-bootstrap';

export default function Cart() {

	const [orders, setOrders] = useState([]);

	useEffect(() => {

		fetch(`https://e-commerce-ma2l.onrender.com/order/all`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(result => result.json())
			.then(data => {
				// console.log(data);
				setOrders([data].map(order => {
					return (<OrderCard key={order._id} orderProp={order} />)
				}))
			})

	}, []);

	return (
		<Fragment>
			<Container>
				<h1 className='mt-5 text-center text-light'>All Orders</h1>
				<Row>
					{orders}
				</Row>
			</Container>
		</Fragment>
	)
}
