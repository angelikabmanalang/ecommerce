import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import { Fragment } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { user, useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';

export default function Register() {
	const [fullName, setFullName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (fullName !== '' && email !== '' && password !== '' && confirmPassword !== '' && password === confirmPassword) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [fullName, email, password, confirmPassword])

	function register(event) {
		event.preventDefault();

		fetch(`https://e-commerce-ma2l.onrender.com/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				fullName: fullName,
				email: email,
				password: password,
				confirmPassword: confirmPassword
			})
		})
			.then(result => result.json())
			.then(data => {
				// console.log(data)

				if (!data) {
					Swal.fire({
						title: 'Registration failed!',
						icon: 'error',
						text: 'Please try again!'
					})

				} else {
					localStorage.setItem('token', data.auth);

					Swal.fire({
						title: 'Registration succesful!',
						icon: 'success',
						text: 'Welcome to JavaSweet!'
					})
					navigate('/login');
				}
			})
	}
	return (
		user ?
			<Navigate to='*' />
			:
			<Container>
				<Row className="mt-5">
					<Col className='navb col-md-4 col-10 ms-auto p-3'>
						<Fragment>
							<h1 className='text-center'>Register</h1>
							<Form className='mt-4' onSubmit={event => register(event)}>

								<Form.Group className="mb-3" controlId="formBasicFullName">
									<Form.Label>Full Name</Form.Label>
									<Form.Control
										type="name"
										placeholder="Enter your full name"
										value={fullName}
										onChange={event => setFullName(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group className="mb-3" controlId="formBasicEmail">
									<Form.Label>Email</Form.Label>
									<Form.Control
										type="email"
										placeholder="Enter your email"
										value={email}
										onChange={event => setEmail(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group className="mb-3" controlId="formBasicPassword">
									<Form.Label>Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Password"
										value={password}
										onChange={event => setPassword(event.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group className="mb-3" controlId="formBasicConfirmPassword">
									<Form.Label>Confirm Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Confirm your password"
										value={confirmPassword}
										onChange={event => setConfirmPassword(event.target.value)}
										required
									/>
								</Form.Group>

								{
									isActive ?
										<Button variant="secondary" className="mx-auto" onClick={event => register(event)}>
                                            Submit
                                        </Button>			
										:
										<Button variant="dark" type="submit" disabled>
											Submit
										</Button>
								}


							</Form>
						</Fragment>
					</Col>
				</Row>
			</Container>
	);
}
