import { Row, Col, Button, Card } from 'react-bootstrap';
import { useState, useContext } from 'react';

import UserContext from '../UserContext.js'
import { Link } from 'react-router-dom';

export default function OrderCard({ orderProp }) {

	const { fullName, productName, quantity } = orderProp;
	const [isDisabled, setIsDisabled] = useState(false);
	const { user } = useContext(UserContext);

	return (
		<Row className="mt-5 text-dark bg-none">
			<Col lg={{ span: 6, offset: 3 }}>
				<Card className='card'>
					<Card.Body>
						<Card.Subtitle>Customer Name:</Card.Subtitle>
						<Card.Text>{fullName}</Card.Text>
						<Card.Subtitle>Item:</Card.Subtitle>
						<Card.Text>{productName}</Card.Text>
						<Card.Subtitle>Quantity:</Card.Subtitle>
						<Card.Text>{quantity}</Card.Text>

						{
							user ?
								<Button as={Link} to={`/order`} className='cardbtn' disabled={isDisabled}>Add to Cart</Button>
								:
								<Button as={Link} to="/login">Login</Button>
						}

					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
