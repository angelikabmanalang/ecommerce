import React from 'react'
import { useState, Link, Fragment, useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import { Form, Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import UserContext from '../UserContext.js';
import { Navigate, useNavigate } from 'react-router-dom';

export default function ArchiveProduct() {

  const { productId } = useParams();
  const [isAvailable, setIsAvailable] = useState('');
  const [isActive, setIsActive] = useState(true)
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    // console.log(productId)
  }, [setIsAvailable]);

  function archive(event) {
    event.preventDefault();

    fetch(`https://e-commerce-ma2l.onrender.com/products/archive/${productId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isAvailable: isAvailable
      })
    })
      .then(result => result.json())
      .then(data => {

        if (data) {
          Swal.fire({
            title: 'Product has been archived!',
            icon: 'success'
          })
          navigate('/products/active');
        } else {
          Swal.fire({
            title: 'Archive failed.',
            icon: 'error'
          })
        }
      })
      // .catch(err => console.log(err));
  }

  return (
    <Container>
      <Row className="mt-5">
        <Col className='loginBg col-md-4 col-10 ms-auto p-3'>
          <Fragment>
            <h1 className='text-center'>Activate Product?</h1>
            <Form className='mt-4' onSubmit={event => archive(event)}>
              <Form.Group className="mb-3" controlId="formGroupIsAvailable">
                <Form.Label></Form.Label>
                <Form.Control
                  type="isAvailable"
                  placeholder="true or false"
                  value={isAvailable}
                  onChange={event => setIsAvailable(event.target.value)}
                  required />
              </Form.Group>
              {
                isActive ?
                  <Button variant="secondary" className="mx-auto" onClick={event => archive(event)}>
                    Submit
                  </Button>
                  :
                  <Button variant="dark" type="submit" disabled>
                    Submit
                  </Button>
              }
            </Form>
          </Fragment>
        </Col>
      </Row>
    </Container>
  )
}