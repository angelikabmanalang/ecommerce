import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext.js';

export default function Single() {
	// const { name, description, price } = productProp;

	const { productId } = useParams();
	const [products, setProducts] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {
		// console.log(productId)
		fetch(`https://e-commerce-ma2l.onrender.com/products/${productId}`)
			.then(result => result.json())
			.then(data => {
				// console.log([data]);
				setProducts([data].map(product => {
					return (<ProductCard key={product._id} productProp={product} />)
				}))
			})
	}, []);

	return (
		<Fragment>
			<Container>
				<h3 className='mt-5 text-center text-light'>Products</h3>
				<Row>
					<Col>
						{products}
						{
							user && !user.isAdmin ?
								<Fragment>
									<Button as={Link} to="/order" className='m-2'>Add to Cart</Button>
									<Button as={Link} to="/products/active" className='m-2'>Back</Button>
								</Fragment>
								:
								<Fragment>
									<Button variant="light" as={Link} to={`/products/update/${productId}`} className='m-2'>Edit Product</Button>
									<Button variant="light" as={Link} to={`/products/archive/${productId}`} className='m-2'>Archive</Button>
								</Fragment>
						}
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}