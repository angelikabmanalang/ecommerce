import React from 'react'
import { useState, Link, Fragment, useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import { Form, Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import UserContext from '../UserContext.js';
import { Navigate, useNavigate } from 'react-router-dom';

export default function UpdateProduct() {

	const { productId } = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(true)
	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		// console.log(productId)
	}, [setName, setDescription, setPrice]);

	function update(event) {
		event.preventDefault();

		fetch(`https://e-commerce-ma2l.onrender.com/products/update/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
			.then(result => result.json())
			.then(data => {

				if (data) {
					Swal.fire({
						title: 'Product updated!',
						icon: 'success'
					})
					navigate('/products');
				} else {
					Swal.fire({
						title: 'Update failed.',
						icon: 'error'
					})
				}
			})
			// .catch(err => console.log(err));
	}

	return (
		<Container>
			<Row className="mt-5">
				<Col className='loginBg col-md-4 col-10 ms-auto p-3'>
					<Fragment>
						<h1 className='text-center'>Edit Product</h1>
						<Form className='mt-4' onSubmit={event => update(event)}>
							<Form.Group className="mb-3" controlId="formGroupProductName">
								<Form.Label>Name</Form.Label>
								<Form.Control
									type="name"
									placeholder="Enter Product Name"
									value={name}
									onChange={event => setName(event.target.value)}
									required />
							</Form.Group>
							<Form.Group className="mb-3" controlId="formGroupDescription">
								<Form.Label>Description</Form.Label>
								<Form.Control
									type="description"
									placeholder="Description"
									value={description}
									onChange={event => setDescription(event.target.value)}
									required />
							</Form.Group>
							<Form.Group className="mb-3" controlId="formGroupPrice">
								<Form.Label>Price</Form.Label>
								<Form.Control
									type="price"
									placeholder="Price"
									value={price}
									onChange={event => setPrice(event.target.value)}
									required />
							</Form.Group >
							{
								isActive ?
									<Fragment>
										<Button variant="secondary" className="mx-auto" onClick={event => update(event)}>
											Submit
										</Button>
									</Fragment>

									:
									<Button variant="dark" type="submit" disabled>
										Submit
									</Button>
							}
						</Form>
					</Fragment>
				</Col>
			</Row>
		</Container>
	)
}