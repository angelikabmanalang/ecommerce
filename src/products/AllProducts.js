import ProductCard from '../components/ProductCard.js';
import { Fragment, useEffect, useState } from 'react';
import { Container, Row } from 'react-bootstrap';

export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {

		fetch(`https://e-commerce-ma2l.onrender.com/products/`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(result => result.json())
			.then(data => {
				console.log(data);
				setProducts(data.map(product => {
					return (<ProductCard key={product._id} productProp={product} />)
				}))
			})

	}, []);

	return (
		<Fragment>
			<Container>
				<h1 className='mt-5 text-center text-light'>All Products</h1>
				<Row>
					{products}
				</Row>
			</Container>
		</Fragment>
	)
}
