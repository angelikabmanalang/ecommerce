import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import { Fragment} from 'react';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';


export default function AddProduct() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const navigate = useNavigate();
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (name !== '' && description !== '' && price !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [name, description, price])

	function add(event) {
		event.preventDefault();

		fetch(`https://e-commerce-ma2l.onrender.com/products/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
			.then(result => result.json())
			.then(data => {
				// console.log(data)

				if (data === true) {
					Swal.fire({
						title: 'Product successfully added!',
						icon: 'success',
						text: 'View your products!'
					})
					navigate("/dashboard");
				} else {

					Swal.fire({
						title: 'Oops! There was an error in adding a product',
						icon: 'error',
						text: 'Please try again!'
					})
				}
			})
			// .catch(err => console.log(err))
	}
	return (
		<Container>
			<Row className="mt-5">
				<Col className='addP col-md-4 col-10 ms-auto p-3'>
					<Fragment>
						<h1 className='text-center'>Add Product</h1>
						<Form className='mt-4' onSubmit={event => add(event)}>
							<Form.Group className="mb-3" controlId="formBasicName">
								<Form.Label>Name</Form.Label>
								<Form.Control
									type="namel"
									placeholder="Enter product name"
									value={name}
									onChange={event => setName(event.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicDescription">
								<Form.Label>Description</Form.Label>
								<Form.Control
									type="description"
									placeholder="Enter product description"
									value={description}
									onChange={event => setDescription(event.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicPrice">
								<Form.Label>Price</Form.Label>
								<Form.Control
									type="price"
									placeholder="Enter product price"
									value={price}
									onChange={event => setPrice(event.target.value)}
									required
								/>
							</Form.Group>
							{
								isActive ?
									<Button variant="secondary" type="submit" className="mx-auto">
										Submit
									</Button>
									:
									<Button variant="dark" type="submit" disabled>
										Submit
									</Button>
							}
						</Form>
					</Fragment>
				</Col>
			</Row>
		</Container>
	);
}
